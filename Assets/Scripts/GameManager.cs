﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Collider colliderExit;

	public Collider colliderBadGuy0;
	public Collider colliderBadGuy1;
	public Collider colliderBadGuy2;
	public Collider colliderBadGuy3;

	public SmileyGuy smileyGuy;
	public GameObject victoryPanel;

	public Text victoryPanelTimeText;
	public Text timeText;

	public Timer timer;

	// Use this for initialization
	void Start () {
		victoryPanel.SetActive(false);
		smileyGuy.onEventTriggerEnter += HandleSmileyGuyTriggerEnter;
	}

	void HandleSmileyGuyTriggerEnter(Collider collider) {
		if (collider == colliderExit) {
			EndGame();
		}
		if ((collider == colliderBadGuy0) || (collider == colliderBadGuy1) || (collider == colliderBadGuy2) || (collider == colliderBadGuy3)) {
			Reset();
		}
	}
	
	void EndGame() {
		timer.enabled = false;
		smileyGuy.Kill();
		victoryPanel.SetActive(true);
	}

	void Reset() {
        timer.counter = 0;
		smileyGuy.Reset();
	}

	// Update is called once per frame
	void Update () {
		victoryPanelTimeText.text = timer.timeInSeconds;
		timeText.text = timer.timeInSeconds;
	}

}