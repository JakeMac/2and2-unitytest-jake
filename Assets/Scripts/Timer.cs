﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	public bool active;
	public string timeInSeconds;
    public int counter = 0;

    // Use this for initialization
    void Start () {
		active = true;
	}
	
	// Update is called once per frame
	void Update () {
        counter++;
        timeInSeconds = (counter / 60) + " seconds";
        

    }

}